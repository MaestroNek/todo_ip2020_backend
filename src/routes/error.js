const express = require('express');
const router = express.Router();

router.use((req, res) => {
    if (req.error) {
        console.log(error);
        res.status(500).json({
            status: 0,
            error: 'Неизвестная ошибка',
        });
    } else {
        res.status(404).json({
            status: 0,
            error: "Не найдено",
        })
    }
})

module.exports.errorRouter = router