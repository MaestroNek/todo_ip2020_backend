const {testingRoute} = require("../functions/test");
const {responseHandler} = require("../middlewares/responeHandler");
const {Router} = require('express')

const router = Router();

router.get('/', responseHandler(testingRoute))

module.exports = router;