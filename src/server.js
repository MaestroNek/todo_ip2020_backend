const express = require('express');
const bodyParser = require('body-parser');
const {errorRouter} = require("./routes/error");
const testRouter = require("./routes/test");
const sequelize = require("./models/connection");

class Server {
    app;

    constructor() {
        this.app = express();
        this.config();

        sequelize
            .sync()
            .then(async () => {
                console.info('DB Sync has been completed.');
            })
    }

    config() {
        this.app.use(bodyParser.json());
        this.app.use('/test', testRouter);
        this.app.use(errorRouter);
    }
}

const PORT = 3000;

new Server().app.listen(PORT, () => {
    console.log('Server listening on port ' + PORT);
});
