const {Sequelize} = require('sequelize');
const {config} = require('dotenv');

config({
    path: `${__dirname}/../../.env${process.env.NODE_ENV === 'dev' ? '.local' : ''}`
});

const sequelize = new Sequelize({
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    dialect: "postgres",
    logging: false,
    models: [__dirname + '/**/*.model.js']
});

sequelize
    .authenticate()
    .then(() => {
        console.log('DB connection has been established');
    })
    .catch(e => {
        console.log(e);
        process.exit(0);
    });

module.exports = sequelize;
