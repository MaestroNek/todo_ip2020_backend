module.exports.responseHandler = (func) => {
    return async function (req, res, next) {
        func(req, res)
            .then(resp => {
                if (resp.status !== -1) {
                    res.status(200).json(resp);
                }
            })
            .catch(e => {
                req.error = e;
                next();
            })
    }
}