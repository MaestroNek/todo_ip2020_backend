module.exports = {
    apps : [{
        name: 'backend',
        script: 'node',
        args: 'src/server.js',
        time: true,
        watch: true,
    }],
};
